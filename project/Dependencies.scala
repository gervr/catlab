import sbt._

object Dependencies {

  private val mainScalaVersion = "3.2.2"

  private val catsVersion = "2.9.0"
  private val catsEffectVerion = "3.4.8"
  private val monocleVersion = "3.2.0"
  private val scalaCheckVersion = "3.2.11.0"
  private val scalaTestVersion = "3.2.15"

  private val catsCore = "org.typelevel" %% "cats-core" % catsVersion
  private val catsKernel = "org.typelevel" %% "cats-kernel" % catsVersion
  private val catsEffect = "org.typelevel" %% "cats-effect" % catsEffectVerion

  private val monocleCore = "dev.optics" %% "monocle-core" % monocleVersion
  private val monocleMacro = "dev.optics" %% "monocle-macro" % monocleVersion

  private val scalaCheck = "org.scalatestplus" %% "scalacheck-1-15" % scalaCheckVersion
  private val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion

  private val cats = Seq(catsCore, catsEffect, catsKernel)
  private val optics = Seq(monocleCore, monocleMacro)
  private val testing = Seq(scalaTest, scalaCheck).map(_ % Test)

  object CatLab {
    val scalaVersion: String = mainScalaVersion
    val libraryDependencies: Seq[ModuleID] = cats ++ optics ++ testing
  }

}
