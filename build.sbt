import Dependencies._

ThisBuild / scalaVersion := CatLab.scalaVersion
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "utf8",
  "-explain",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings"
)

lazy val root = project
  .in(file("."))
  .aggregate(catlab)
  .settings(
    name := "catlab"
  )

lazy val catlab = project
  .in(file("catlab"))
  .settings(
    name := "catlab",
    libraryDependencies ++= CatLab.libraryDependencies
  )