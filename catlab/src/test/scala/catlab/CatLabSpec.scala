package catlab

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CatLabSpec extends AnyWordSpec with Matchers {

  "CatLab" should {
    "Provide a friendly message" in {
      CatLab.message shouldBe "Hello from CatLab!"
    }
  }

}
