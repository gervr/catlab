package catlab.laws

import catlab.parser.ParserAlgebra
import catlab.parser.ParserAlgebra.*
import org.scalacheck.Prop.*
import org.scalacheck.{Gen, Prop}

trait ParsersLaws extends ParserAlgebra {

  def equal[A](p1: Parser[A], p2: Parser[A])(in: Gen[String]): Prop =
    forAll(in)(s => run(p1)(s) == run(p2)(s))

  def mapLaw[A](p: Parser[A])(in: Gen[String]): Prop =
    equal(p, p.map(a => a))(in)

}
