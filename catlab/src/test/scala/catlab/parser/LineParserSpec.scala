package catlab.parser

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.util.Success

class LineParserSpec extends AnyWordSpec with Matchers {

  import LineParser.*
  import ParserAlgebra.syntax.*

  "LineParser" should {
    "Have a semicolon field parser" in {
      semicolonFieldParser.run("abc;def") shouldBe Success("abc")
    }
  }

}
