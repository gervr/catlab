package catlab

object CatLab {

  val message: String = "Hello from CatLab!"

  @main def run(): Unit =
    println(message)

}
