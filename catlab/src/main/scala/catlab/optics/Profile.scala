package catlab.optics

import monocle.syntax.all.*
import monocle.{AppliedIso, AppliedLens}

import java.text.Normalizer

case class Profile(client: Client, nProducts: Int) {}

case class Client(name: String, address: Address)
case class Address(street: Street, place: String)
case class Street(name: String, number: Int)

object Profile {

  def normalize(s: String): String =
    Normalizer
      .normalize(s, Normalizer.Form.NFKD)
      .replaceAll("[^\\p{ASCII}]", "")
      .trim
      .replaceAll("\\s+", " ")
      .toUpperCase()

  def normalizeProfile1(profile: Profile): Profile =
    profile.copy(
      client = profile.client.copy(
        address = profile.client.address.copy(
          place = normalize(profile.client.address.place),
          street = profile.client.address.street.copy(
            name = normalize(profile.client.address.street.name)
          )
        )
      )
    )

  def normalizeProfile2(profile: Profile): Profile =
    profile
      .focus(_.client.address.place)
      .modify(normalize)
      .focus(_.client.address.street.name)
      .modify(normalize)

  private val placeFocus = (p: Profile) => p.focus(_.client.address.place)
  private val normalizePlace = placeFocus(_: Profile).modify(normalize)

  private val streetNameFocus = (p: Profile) => p.focus(_.client.address.street.name)
  private val normalizeStreetName = streetNameFocus(_: Profile).modify(normalize)

  private val normalizeProfile3 = normalizePlace andThen normalizeStreetName

  def main(args: Array[String]): Unit = {
    val a = Profile(Client("Alice", Address(Street("Hóòfdstrâât", 1), "Ons \tfĳne  Dörp \t")), 3)
    val b = a.focus(_.client.name).replace("Bob")

    println(s"original profiles: $a")
    println()
    println(s"cleaned profile 1: ${normalizeProfile1(a)}")
    println(s"cleaned profile 2: ${normalizeProfile2(a)}")
    println(s"cleaned profile 3: ${normalizeProfile3(a)}")
    println()
    println(s"original profiles: $a")
    println(s"                 : $b")
  }

}
