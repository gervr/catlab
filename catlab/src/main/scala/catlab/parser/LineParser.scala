package catlab.parser

import catlab.parser.ParserAlgebra.*
import catlab.parser.ParserAlgebra.syntax.*

import scala.util.Success

object LineParser {

  val empty: String = ""

  private def split(s: String, separator: Char): (String, String) = {
    val idx = s.indexOf(separator)
    if (idx >= 0) (s.substring(0, idx), s.substring(idx + 1))
    else (s, empty)
  }

  def delimitedFieldParser(separator: Char): Parser[String] =
    input => Success(split(input, separator))

  val semicolonFieldParser: Parser[String] = delimitedFieldParser(';')

}
