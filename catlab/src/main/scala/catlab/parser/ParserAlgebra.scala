package catlab.parser

import catlab.parser.ParserAlgebra.Parser

import scala.language.implicitConversions
import scala.util.control.NoStackTrace
import scala.util.{Failure, Success, Try}

trait ParserAlgebra { self =>

  def run[A](parser: Parser[A])(input: String): Try[A]

  def map[A, B](parser: Parser[A])(function: A => B): Parser[B]

  def flatMap[A, B](parser: Parser[A])(function: A => Parser[B]): Parser[B]

  def withFilter[A](parser: Parser[A])(condition: A => Boolean): Parser[A]

  implicit def operators[A](parser: Parser[A]): Ops[A] = Ops(parser)

  case class Ops[A](parser: Parser[A]) {

    def run(input: String): Try[A] = self.run(parser)(input)

    def map[B](function: A => B): Parser[B] = self.map(parser)(function)

    def flatMap[B](function: A => Parser[B]): Parser[B] = self.flatMap(parser)(function)

    def withFilter(condition: A => Boolean): Parser[A] = self.withFilter(parser)(condition)

  }

}

object ParserAlgebra {

  type Parser[A] = String => Try[(A, String)]

  case class ParseError(message: String) extends Throwable(message) with NoStackTrace

  val syntax: ParserAlgebra = new ParserAlgebra {

    override def run[A](parser: Parser[A])(input: String): Try[A] =
      parser(input).map(_._1)

    override def map[A, B](parser: Parser[A])(function: A => B): Parser[B] =
      input => parser(input).map { case (a, rest) => (function(a), rest) }

    override def flatMap[A, B](parser: Parser[A])(function: A => Parser[B]): Parser[B] =
      input => parser(input).flatMap { case (a, rest) => function(a)(rest) }

    override def withFilter[A](parser: Parser[A])(condition: A => Boolean): Parser[A] =
      input =>
        parser(input).flatMap { case (result, rest) =>
          if (condition(result)) Success((result, rest))
          else Failure(ParseError(s"filter condition failed on '$result'"))
        }

  }

}
