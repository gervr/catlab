import catlab.parser.LineParser.*
import catlab.parser.ParserAlgebra.syntax.*

val readString = delimitedFieldParser(';')

readString.run("abc;123;4.56;xyz")
readString("abc;123;4.56;xyz")

val readInt = readString.map(_.toInt)
readInt.run("-123;xyz")
readInt.run("abc;xyz")

val readDouble = readString.map(_.toDouble)

case class Demo(s1: String, i: Int, d: Double, s2: String)

val demoParser = for {
  s1 <- readString
  i <- readInt if i > 0
  d <- readDouble if d > i
  s2 <- readString
} yield Demo(s1, i, d, s2)

demoParser.run("abc;123;456;xyz")

demoParser.run("abc;-123;4.56;xyz")
demoParser.run("abc;123;4.56;xyz")
